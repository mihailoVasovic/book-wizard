import React from 'react';
import styled from 'styled-components';
import Step from '../../components/Step/Step';

const StyledStepper = styled.div`
  width: 100%;
  margin: 0 0 20px 0;
  padding: 0;
  font-size: 0;
`;

const Stepper = props => {
  return (
    <StyledStepper>
      {props.items.map((item, i) => (
        <Step
          key={i}
          length={props.items.length}
          name={item.name}
          step={item.step}
          selected={item.selected}
        />
      ))}

      {props.children}
    </StyledStepper>
  );
};

export default Stepper;
