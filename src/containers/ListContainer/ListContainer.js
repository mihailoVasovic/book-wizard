import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import List from '../../components/List/List';

import { getGenres } from '../../store/reducers/genres';

import {
  newSubgenreSelected,
  getGenre,
  getSubgenre
} from '../../store/reducers/form';
import {
  selectNewSubgenre,
  setGenre,
  setSubgenre
} from '../../store/actions/form';

import { getSteps, getStep } from '../../store/reducers/stepper';

class ListContainer extends Component {
  renderGenres = () => {
    return (
      <List
        items={this.props.genres}
        click={this.props.setGenre}
        selected={this.props.genre.id}
        step={this.props.step}
      />
    );
  };

  renderSubgenres = () => {
    const subgenres = this.props.genres
      .find(el => el.id === this.props.genre.id)
      .subgenres.concat([
        {
          name: 'Add New',
          id: '',
          isNew: true
        }
      ]);
    return (
      <List
        items={subgenres}
        click={this.props.setSubgenre}
        selectNewSubgenre={this.props.selectNewSubgenre}
        newSubgenreSelected={this.props.newSubgenreSelected}
        selected={this.props.subgenre.id}
        step={this.props.step}
      />
    );
  };

  render() {
    switch (this.props.selectedType) {
      case 'genre':
        return this.renderGenres();
      case 'subgenre':
        return this.renderSubgenres();
      default:
        break;
    }
  }
}

const mapStateToProps = state => ({
  genres: getGenres(state),
  steps: getSteps(state),
  genre: getGenre(state),
  subgenre: getSubgenre(state),
  step: getStep(state),
  newSubgenreSelected: newSubgenreSelected(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setGenre,
      setSubgenre,
      selectNewSubgenre
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListContainer);
