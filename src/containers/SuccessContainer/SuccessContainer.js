import React from 'react';
import styled from 'styled-components';
import Button from '../../components/Button/Button';
const StyledSuccessContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 50px 0 20px 0;
  justify-content: flex-start;
  width: 100%;
`;

const StyledSuccessCircle = styled.div`
  background-color: #e7ecf2;
  width: 150px;
  height: 150px;
  border-radius: 50%;
`;

const StyledIcon = styled.svg`
  fill: none;
  stroke: #a0acb8;
  stroke-width: 1px;
`;

const StyledText = styled.p`
  font-weight: 500;
  font-size: 18px;
  margin-top: 20px;
`;

const SuccessContainer = props => {
  return (
    <StyledSuccessContainer>
      <StyledSuccessCircle>
        <StyledIcon viewBox="0 0 24 24">
          <polyline points="20 6 9 17 4 12" />
        </StyledIcon>
      </StyledSuccessCircle>
      <StyledText>Book added successfully</StyledText>
      <Button
        name="Add another book"
        width="50%"
        padding="7px"
        color="#fff"
        background="#515a64"
      />
    </StyledSuccessContainer>
  );
};

export default SuccessContainer;
