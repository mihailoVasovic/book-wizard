import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getGenres } from '../../store/reducers/genres';

import { getSteps, getStep } from '../../store/reducers/stepper';

import {
  getAuthors,
  getPublishers,
  getLanguages,
  getFormats
} from '../../store/reducers/informations';

import {
  setInfo,
  setSubgenre,
  selectNewSubgenre
} from '../../store/actions/form';
import {
  getGenre,
  getInfo,
  getSubgenre,
  newSubgenreSelected
} from '../../store/reducers/form';

import Field from '../../components/Field/Field';
import Textarea from '../../components/Textarea/Textarea';
import Checkbox from '../../components/Checkbox/Checkbox';
import Select from '../../components/Select/Select';
import Wrapper from '../../containers/Wrapper/Wrapper';

class FormContainer extends Component {
  renderNewSubgenreForm = () => {
    return (
      <Wrapper border="none">
        <Field
          isRequired={true}
          placeholder="Name"
          value={this.props.subgenre.name}
          change={this.props.setSubgenre}
          id="name"
        />
        <Textarea
          value={this.props.subgenre.subgenreDescription}
          placeholder="Description"
          id="subgenreDescription"
          isRequired={this.props.subgenre.isDescriptionRequired}
          change={this.props.setSubgenre}
        />
        <Checkbox
          value={this.props.subgenre.isDescriptionRequired}
          placeholder=""
          id="isDescriptionRequired"
          checked={this.props.subgenre.isDescriptionRequired}
          change={this.props.setSubgenre}
        />
      </Wrapper>
    );
  };

  renderInfoForm = () => {
    const formFields = [
      {
        placeholder: 'Book title',
        value: this.props.info.title,
        id: 'title',
        type: 'field'
      },
      {
        placeholder: 'Author',
        value: this.props.info.author,
        options: this.props.authors,
        id: 'author',
        type: 'select'
      },
      {
        placeholder: 'ISBN',
        value: this.props.info.isbn,
        id: 'isbn',
        type: 'field'
      },
      {
        placeholder: 'Publisher',
        value: this.props.info.publisher,
        options: this.props.publishers,
        id: 'publisher',
        type: 'select'
      },
      {
        placeholder: 'Date published',
        value: this.props.info.published,
        id: 'published',
        type: 'field',
        width: '30%'
      },
      {
        placeholder: 'Number of pages',
        value: this.props.info.pages,
        id: 'pages',
        type: 'field',
        width: '21%'
      },
      {
        placeholder: 'Format',
        value: this.props.info.format,
        options: this.props.formats,
        id: 'format',
        type: 'select',
        width: '30%'
      },
      {
        placeholder: 'Edition',
        value: this.props.info.edition,
        id: 'edition',
        type: 'field',
        containerWidth: '30%',
        marginRight: '30px',
        display: 'inline-block'
      },
      {
        placeholder: 'Edition language',
        value: this.props.info.language,
        options: this.props.languages,
        id: 'language',
        type: 'select',
        width: '100%',
        containerWidth: '30%',
        display: 'inline-block'
      },
      {
        placeholder: 'Description',
        value: this.props.info.infoDescription,
        id: 'infoDescription',
        type: 'textarea'
      }
    ];

    return (
      <Wrapper border="none" padding="0">
        {formFields.map((field, i) => {
          switch (field.type) {
            case 'field':
              return (
                <Field
                  key={i}
                  isRequired={true}
                  value={field.value}
                  placeholder={field.placeholder}
                  change={this.props.setInfo}
                  id={field.id}
                  width={field.width ? field.width : '100%'}
                  containerWidth={
                    field.containerWidth ? field.containerWidth : '100%'
                  }
                  display={field.display ? field.display : null}
                  marginRight={field.marginRight ? field.marginRight : null}
                />
              );
            case 'select':
              return (
                <Select
                  key={i}
                  isRequired={true}
                  value={field.value}
                  placeholder={field.placeholder}
                  change={this.props.setInfo}
                  id={field.id}
                  options={field.options}
                  width={field.width ? field.width : '100%'}
                  containerWidth={
                    field.containerWidth ? field.containerWidth : '100%'
                  }
                  display={field.display ? field.display : null}
                  marginRight={field.marginRight ? field.marginRight : null}
                />
              );
            case 'textarea':
              return (
                <Textarea
                  key={i}
                  isRequired={true}
                  value={field.value}
                  placeholder={field.placeholder}
                  change={this.props.setInfo}
                  id={field.id}
                  subgenreId={this.props.subgenre.id}
                />
              );
            default:
              return field;
          }
        })}
      </Wrapper>
    );
  };

  render() {
    switch (this.props.selectedType) {
      case 'newSubgenre':
        return this.renderNewSubgenreForm();
      case 'info':
        return this.renderInfoForm();
      default:
        break;
    }
  }
}

const mapStateToProps = state => ({
  genres: getGenres(state),
  steps: getSteps(state),
  genre: getGenre(state),
  subgenre: getSubgenre(state),
  info: getInfo(state),
  step: getStep(state),
  authors: getAuthors(state),
  publishers: getPublishers(state),
  languages: getLanguages(state),
  formats: getFormats(state),
  newSubgenreSelected: newSubgenreSelected(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setInfo,
      setSubgenre,
      selectNewSubgenre
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormContainer);
