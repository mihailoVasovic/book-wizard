import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  padding: ${props => (props.padding ? props.padding : '16px')};
  border: ${props => (props.border ? props.border : '1px solid #ddd')};
  margin: 0;
  border-radius: 5px;
`;

const Wrapper = props => {
  return (
    <StyledWrapper
      border={props.border}
      padding={props.padding}
      onClick={props.click}
    >
      {props.children}
    </StyledWrapper>
  );
};

export default Wrapper;
