import React from 'react';
import styled from 'styled-components';
import Button from '../../components/Button/Button';

const StyledFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 60px;
`;

const Footer = props => {
  const validateNextButton = () => {
    switch (props.step) {
      case 0:
        return Object.values(props.genre).some(val => !!val);
      case 1:
        return (
          Object.values(props.subgenre).some(val => !!val) ||
          props.newSubgenreSelected
        );
      case 2:
        if (props.steps.length > 3) {
          return props.subgenre.isDescriptionRequired
            ? props.subgenre.name && props.subgenre.description
            : props.subgenre.name;
        } else {
          return Object.values(props.info).every(val => !!val);
        }
      case 3:
        return Object.values(props.info).every(val => !!val);

      default:
        break;
    }
  };

  const disabled = validateNextButton();

  return (
    <StyledFooter>
      <Button
        padding="5px 35px 5px 35px"
        margin="0 10px 0 0"
        click={props.handlePrevious}
        disabled={props.step === 0}
        name="Back"
      />

      <Button
        name={props.steps[props.step].id === 'info' ? 'Add' : 'Next'}
        padding="5px 35px 5px 35px"
        background={!disabled ? '#cccccc' : '#505a64'}
        color={!disabled ? '#505a64' : '#e8e8e8'}
        disabled={!disabled}
        click={
          props.steps[props.step].id === 'info'
            ? props.showResults
            : props.handleNext
        }
      />
    </StyledFooter>
  );
};

export default Footer;
