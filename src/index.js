import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import './index.scss';
import App from './components/App/App';
import reducer from './store/reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk),
    composeWithDevTools()
  )
);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
