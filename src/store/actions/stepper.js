export const NEXT_STEP = 'NEXT_STEP';
export const PREVIOUS_STEP = 'PREVIOUS_STEP';
export const ADD_SUBGENRE = 'ADD_SUBGENRE';

export const nextStep = step => {
  return {
    type: NEXT_STEP,
    payload: step
  };
};

export const previousStep = () => ({
  type: PREVIOUS_STEP
});

export const addSubgenre = () => ({
  type: ADD_SUBGENRE
});
