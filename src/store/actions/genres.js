import axios from 'axios';

export const FETCH_GENRES_PENDING = 'FETCH_GENRES_PENDING';
export const FETCH_GENRES_SUCCESS = 'FETCH_GENRES_SUCCESS';
export const FETCH_GENRES_FAIL = 'FETCH_GENRES_FAIL';

export const fetchGenresPending = () => ({
  type: FETCH_GENRES_PENDING
});

export const fetchGenresSuccess = genres => ({
  type: FETCH_GENRES_SUCCESS,
  payload: genres
});

export const fetchGenresFail = error => ({
  type: FETCH_GENRES_FAIL,
  payload: error
});

export const fetchGenres = () => {
  return dispatch => {
    dispatch(fetchGenresPending());
    return axios
      .get('./genres.json')
      .then(response => {
        dispatch(fetchGenresSuccess(response.data));
      })
      .catch(err => {
        dispatch(fetchGenresFail(err));
      });
  };
};
