export const SET_NEW_SUBGENRE_COLOR = 'SET_NEW_SUBGENRE_COLOR';
export const SET_INFO_COLOR = 'SET_INFO_COLOR';

export const setNewSubgenreColor = payload => ({
  type: SET_NEW_SUBGENRE_COLOR,
  payload: payload
});

export const setInfoColor = payload => ({
  type: SET_INFO_COLOR,
  payload: payload
});
