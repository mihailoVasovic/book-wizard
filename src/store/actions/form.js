export const SET_GENRE = 'SET_GENRE';
export const SET_SUBGENRE = 'SET_SUBGENRE';
export const SET_INFO = 'SET_INFO';
export const SELECT_NEW_SUBGENRE = 'SELECT_NEW_SUBGENRE';

export const setGenre = payload => ({
  type: SET_GENRE,
  payload: payload
});

export const setSubgenre = payload => ({
  type: SET_SUBGENRE,
  payload: payload
});

export const setInfo = payload => ({
  type: SET_INFO,
  payload: payload
});

export const selectNewSubgenre = () => ({
  type: SELECT_NEW_SUBGENRE
});
