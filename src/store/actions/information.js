import axios from 'axios';

export const FETCH_INFORMATIONS_PENDING = 'FETCH_INFORMATIONS_PENDING';
export const FETCH_INFORMATIONS_SUCCESS = 'FETCH_INFORMATIONS_SUCCESS';
export const FETCH_INFORMATIONS_FAIL = 'FETCH_INFORMATIONS_FAIL';

export const fetchInformationsPending = () => ({
  type: FETCH_INFORMATIONS_PENDING
});

export const fetchInformationsSuccess = informations => ({
  type: FETCH_INFORMATIONS_SUCCESS,
  payload: informations
});

export const fetchInformationsFail = error => ({
  type: FETCH_INFORMATIONS_FAIL,
  payload: error
});

export const fetchInformations = () => {
  return dispatch => {
    dispatch(fetchInformationsPending());
    return axios
      .get('./informations.json')
      .then(response => {
        dispatch(fetchInformationsSuccess(response.data));
      })
      .catch(err => {
        dispatch(fetchInformationsFail(err));
      });
  };
};
