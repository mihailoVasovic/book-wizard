import { combineReducers } from 'redux';
import genres from './genres';
import stepper from './stepper';
import form from './form';
import informations from './informations';
import textarea from './textarea';

const reducer = combineReducers({
  genres,
  stepper,
  form,
  informations,
  textarea
});

export default reducer;
