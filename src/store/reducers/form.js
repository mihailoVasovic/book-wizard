const initialState = {
  genre: {
    id: null,
    name: null
  },
  subgenre: {
    id: null,
    name: null,
    isDescriptionRequired: null,
    subgenreDescription: null
  },
  info: {
    title: null,
    author: null,
    isbn: null,
    publisher: null,
    published: null,
    pages: null,
    format: null,
    edition: null,
    language: null,
    infoDescription: null
  },
  newSubgenreSelected: false
};

const form = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_GENRE':
      return {
        ...state,
        genre: { ...action.payload }
      };

    case 'SET_SUBGENRE':
      return {
        ...state,
        newSubgenreSelected: !!!action.payload.id,
        subgenre: { ...state.subgenre, ...action.payload }
      };
    case 'SET_INFO':
      return {
        ...state,
        info: { ...state.info, ...action.payload }
      };
    case 'SELECT_NEW_SUBGENRE':
      return {
        ...state,
        newSubgenreSelected: true
      };

    default:
      return {
        ...state
      };
  }
};
export const newSubgenreSelected = state => state.form.newSubgenreSelected;
export const getGenre = state => state.form.genre;
export const getSubgenre = state => state.form.subgenre;
export const getInfo = state => state.form.info;
export const getForm = state => state.form;
export default form;
