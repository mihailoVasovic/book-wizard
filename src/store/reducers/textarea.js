const initialState = {
  subgenreColor: '#555',

  infoColor: {
    id: null,
    color: '#555'
  }
};

const textarea = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_NEW_SUBGENRE_COLOR':
      return {
        ...state,
        subgenreColor: action.payload
      };
    case 'SET_INFO_COLOR':
      return {
        ...state,
        infoColor: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export const getSubgenreColor = state => state.textarea.subgenreColor;
export const getInfoColor = state => state.textarea.infoColor;
export default textarea;
