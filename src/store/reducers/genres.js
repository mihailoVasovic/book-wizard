const initialState = {
  genres: [],
  error: null,
  pending: false
};

const genres = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_GENRES_PENDING':
      return {
        ...state,
        pending: true
      };
    case 'FETCH_GENRES_SUCCESS':
      return {
        ...state,
        pending: false,
        genres: action.payload.genres
      };
    case 'FETCH_GENRES_FAIL':
      return {
        ...state,
        pending: false,
        error: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export const getGenres = state => state.genres.genres;
export const getPending = state => state.genres.pending;
export const getError = state => state.genres.error;
export default genres;
