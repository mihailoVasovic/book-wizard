const initialState = {
  steps: [
    {
      step: 1,
      selected: true,
      name: 'Genre',
      id: 'genre'
    },
    {
      step: 2,
      selected: false,
      name: 'Subgenre',
      id: 'subgenre'
    },
    {
      step: '...',
      selected: false,
      name: null,
      id: null
    }
  ]
};

const stepper = (state = initialState, action) => {
  let selectedStep;
  switch (action.type) {
    case 'NEXT_STEP':
      selectedStep = state.steps.findIndex(el => el.selected);

      switch (state.steps.length) {
        case 4:
          if (selectedStep === 1 && !!action.payload.subgenreId) {
            state.steps = state.steps
              .filter((_, i) => i < 2)
              .map(el => ({ ...el, selected: false }))
              .concat([
                {
                  step: 3,
                  name: 'Information',
                  selected: true,
                  id: 'info'
                }
              ]);
          } else {
            state.steps = state.steps.map((step, i) => ({
              ...step,
              selected: i - 1 === selectedStep
            }));
          }
          break;

        case 3:
          if (selectedStep === 1 && !!action.payload.subgenreId) {
            state.steps = state.steps
              .filter((_, i) => i < 2)
              .map(el => ({ ...el, selected: false }))
              .concat([
                { step: 3, name: 'Information', selected: true, id: 'info' }
              ]);
          } else if (selectedStep === 1 && !!!action.payload.subgenreId) {
            state.steps = state.steps
              .filter((_, i) => i < 2)
              .map(el => ({ ...el, selected: false }))
              .concat([
                {
                  step: 3,
                  name: 'Add subgenre',
                  selected: true,
                  id: 'newSubgenre'
                },
                { step: 4, name: 'Information', selected: false, id: 'info' }
              ]);
          } else {
            state.steps = state.steps.map((step, i) => ({
              ...step,
              selected: i - 1 === selectedStep
            }));
          }

          break;
        default:
          break;
      }

      return {
        ...state
      };

    case 'PREVIOUS_STEP':
      selectedStep = state.steps.findIndex(el => el.selected);
      return {
        steps: state.steps.map((step, i) => ({
          ...step,
          selected: i === selectedStep - 1
        }))
      };

    default:
      return {
        ...state
      };
  }
};

export const getSteps = state => state.stepper.steps;
export const getStep = state =>
  state.stepper.steps.findIndex(step => step.selected);

export default stepper;
