const initialState = {
  authors: [],
  publishers: [],
  formats: [],
  languages: [],
  error: null,
  pending: false
};

const informations = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_INFORMATIONS_PENDING':
      return {
        ...state,
        pending: true
      };
    case 'FETCH_INFORMATIONS_SUCCESS':
      return {
        ...state,
        pending: false,
        ...action.payload
      };
    case 'FETCH_INFORMATIONS_FAIL':
      return {
        ...state,
        pending: false,
        error: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export const getAuthors = state => state.informations.authors;
export const getPublishers = state => state.informations.publishers;
export const getFormats = state => state.informations.formats;
export const getLanguages = state => state.informations.languages;
export default informations;
