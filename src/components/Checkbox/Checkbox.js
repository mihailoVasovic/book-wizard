import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  display: flex;
  vertical-align: middle;
  margin-top: 15px;
`;

const StyledIcon = styled.svg`
  fill: none;
  stroke: #555;
  stroke-width: 3px;
`;

const StyledOriginalCheckbox = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const StyledCheckbox = styled.label`
  display: inline-block;
  width: 25px;
  height: 25px;
  border-radius: 5px;
  transition: all 150ms;
  border: 2px solid #d6d8da;
  position: relative;

  ${StyledIcon} {
    visibility: ${props => (props.checked ? 'visible' : 'hidden')};
  }
`;

const StyledLabel = styled.div`
  margin-left: 10px;
  font-weight: 500;
`;

const Checkbox = props => {
    /**
   * Handles onChange checkbox event
   * @param evt - true if checked
   */
  const handleChange = evt => props.change({ [props.id]: evt });

  return (
    <StyledContainer>
      <div>
        <StyledOriginalCheckbox
          htmlChecked={props.checked}
          onChange={evt => handleChange(evt.target.checked)}
          id={props.id}
        />
        <StyledCheckbox checked={props.checked} htmlFor={props.id}>
          <StyledIcon viewBox="0 0 24 24">
            <polyline points="20 6 9 17 4 12" />
          </StyledIcon>
        </StyledCheckbox>
      </div>
      <StyledLabel>Description is required form this subgenre</StyledLabel>
    </StyledContainer>
  );
};

export default Checkbox;
