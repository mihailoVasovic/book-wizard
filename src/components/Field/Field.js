import React, { Component } from 'react';
import styled from 'styled-components';
import { DebounceInput } from 'react-debounce-input';

const StyledField = styled.div`
  margin-top: 15px;
  margin-right: ${props => (props.marginRight ? props.marginRight : '0')};
  display: ${props => (props.display ? props.display : 'block')};
  width: ${props => (props.containerWidth ? props.containerWidth : 'inherit')};
`;

const StyledInput = styled.input`
  position: relative;
  width: ${props => (props.width ? props.width : '100%')};
  border: ${props =>
    props.isDirty && props.isRequired && !props.isValid
      ? '2px solid red'
      : '2px solid #d6d8da'};
  border-radius: 5px;
  display: block;
  padding: 5px;
  color: #555;
  font-weight: 500;
  &::placeholder {
    color: #ccc;
  }
  &:focus {
    outline: none;
  }
`;

const StyledLabel = styled.label`
  color: #555;
  font-weight: 500;
  display: block;
`;
const StyledErrorMessage = styled.span`
  color: red;
`;
class Field extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDirty: false,
      error: null
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      error:
        this.state.isDirty && props.isRequired && !props.value
          ? 'Field is required'
          : null
    });
  }

    /**
   * Handles onChange input event
   * @param evt - value of input
   */
  handleChange = evt => {
    this.props.change({ [this.props.id]: evt });
    this.setState({
      isDirty: true,
      error:
        this.props.isRequired && !this.props.value ? 'Field is required' : null
    });
  };

  render() {
    return (
      <StyledField
        display={this.props.display}
        marginRight={this.props.marginRight}
        containerWidth={this.props.containerWidth}
      >
        <StyledLabel>{this.props.placeholder}</StyledLabel>

        <DebounceInput
          isRequired={this.props.isRequired}
          isDirty={this.state.isDirty}
          isValid={!!this.props.value}
          element={StyledInput}
          value={this.props.value ? this.props.value : ''}
          width={this.props.width}
          type="text"
          placeholder={this.props.placeholder}
          onChange={evt => this.handleChange(evt.target.value)}
          debounceTimeout={300}
        />

        <StyledErrorMessage>{this.state.error}</StyledErrorMessage>
      </StyledField>
    );
  }
}

export default Field;
