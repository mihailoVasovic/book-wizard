import React, { Component } from 'react';
import styled from 'styled-components';

const StyledSelect = styled.div`
  margin-top: 15px;
  margin-right: ${props => (props.marginRight ? props.marginRight : '0')};
  display: ${props => (props.display ? props.display : 'block')};
  width: ${props => (props.containerWidth ? props.containerWidth : 'inherit')};
`;

const StyledInput = styled.select`
  position: relative;
  display: block;
  width: ${props => (props.width ? props.width : '100%')};
  border: ${props =>
    props.isDirty && props.isRequired && !props.isValid
      ? '2px solid red'
      : '2px solid #d6d8da'};
  border-radius: 5px;
  padding: 5px;
  color: #555;
  font-weight: 500;
  -moz-appearance: none;
  -webkit-appearance: none;
  background: transparent;
  background-image: url('data:image/svg+xml;utf-8,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.12 9.29L12 13.17l3.88-3.88c.39-.39 1.02-.39 1.41 0 .39.39.39 1.02 0 1.41l-4.59 4.59c-.39.39-1.02.39-1.41 0L6.7 10.7c-.39-.39-.39-1.02 0-1.41.39-.38 1.03-.39 1.42 0z"/></svg>');
  background-repeat: no-repeat;
  background-position-x: 100%;
  background-position-y: 5px;
  &::placeholder {
    color: #ccc;
  }
  &:hover {
    cursor: pointer;
  }
  &:focus {
    outline: none;
  }
`;

const StyledErrorMessage = styled.span`
  color: red;
`;

const StyledLabel = styled.label`
  color: #555;
  font-weight: 500;
  display: block;
`;

const StyledOption = styled.option`
  color: #555;
  font-weight: 500;
`;

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDirty: false,
      error: null
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      error:
        this.state.isDirty && props.isRequired && !props.value
          ? 'Field is required'
          : null
    });
  }

  /**
   * Handles onChange input event
   * @param evt - value of input
   */
  handleChange = evt => {
    
    this.props.change({ [this.props.id]: evt });
    this.setState({
      isDirty: true,
      error:
        this.props.isRequired && !this.props.value ? 'Field is required' : null
    });
  };
  render() {
    return (
      <StyledSelect
        display={this.props.display}
        containerWidth={this.props.containerWidth}
      >
        <StyledLabel>{this.props.placeholder}</StyledLabel>
        <StyledInput
          isRequired={this.props.isRequired}
          isDirty={this.state.isDirty}
          isValid={!!this.props.value}
          width={this.props.width}
          placeholder={this.props.placeholder}
          onChange={evt => this.handleChange(evt.target.value)}
        >
          <option value="">Select {this.props.placeholder}</option>
          {this.props.options.map(option => (
            <StyledOption key={option.id} value={option.id}>
              {option.name}
            </StyledOption>
          ))}
        </StyledInput>
        <StyledErrorMessage>{this.state.error}</StyledErrorMessage>
      </StyledSelect>
    );
  }
}

export default Select;
