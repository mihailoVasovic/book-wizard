import React from 'react';
import styled from 'styled-components';
import Button from '../Button/Button';

const StyledContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 50px 0;
  font-size: 16px;
`;

class List extends React.Component {
  render() {
    const handleClick = item => {
      switch (this.props.step) {
        case 0:
          return this.props.click({ name: item.name, id: item.id });
        case 1:
          if (item.hasOwnProperty('isNew')) {
            this.props.click({
              name: null,
              id: item.id,
              isDescriptionRequired: null
            });
            return this.props.selectNewSubgenre();
          }
          return this.props.click({
            name: item.name,
            id: item.id,
            isDescriptionRequired: item.isDescriptionRequired
          });
        default:
          break;
      }
    };

    return (
      <StyledContent>
        {this.props.items.map(item => (
          <Button
            padding="20px 30px"
            margin="10px 20px 10px 0"
            name={item.name}
            color={this.props.selected === item.id ? '#ccc' : 'inherit'}
            background={this.props.selected === item.id ? '#505a64' : 'inherit'}
            key={item.id}
            click={() => handleClick(item)}
          />
        ))}
      </StyledContent>
    );
  }
}

export default List;
