import React from 'react';
import styled from 'styled-components';

const StyledStepper = styled.div`
  display: table;
  width: 100%;
  margin: 0;
  background-color: #ffffff;
`;

const StyledBar = styled.div`
  position: absolute;
  top: 36px;
  height: 1px;
  border-top: 1px solid #dddddd;
`;
const StyledLeftBar = styled(StyledBar)`
  left: 0;
  right: 50%;
  margin-right: 40px;
`;

const StyledRightBar = styled(StyledBar)`
  right: 0;
  left: 50%;
  margin-left: 40px;
`;

const StyledStep = styled.div`
  display: table-cell;
  position: relative;
  padding: 24px;

  &:first-child ${StyledLeftBar} {
    display: none;
  }
  &:last-child ${StyledRightBar} {
    display: none;
  }
`;

const StyledCircle = styled.div`
  width: 30px;
  height: 30px;
  margin: 0 auto;
  background-color: #999999;
  border-radius: 50%;
  text-align: center;
  line-height: 30px;
  font-size: 16px;
  font-weight: 600;
  color: #ffffff;
`;

const Stepper = ({ steps }) => {
  return (
    <StyledStepper>
      {steps.map((step, i) => {
        return (
          <StyledStep key={i}>
            <StyledCircle>{i + 1}</StyledCircle>
            <StyledLeftBar />
            <StyledRightBar />
          </StyledStep>
        );
      })}
    </StyledStepper>
  );
};

export default Stepper;
