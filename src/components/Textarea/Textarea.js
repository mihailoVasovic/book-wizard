import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import TextFormatIcon from '@material-ui/icons/TextFormat';
import LinkIcon from '@material-ui/icons/Link';
import MentionIcon from '@material-ui/icons/AlternateEmail';
import { DebounceInput } from 'react-debounce-input';
import { GithubPicker } from 'react-color';
import {
  setInfoColor,
  setNewSubgenreColor
} from '../../store/actions/textarea';
import { getInfoColor, getSubgenreColor } from '../../store/reducers/textarea';

const StyledContainer = styled.div`
  margin-top: 15px;
`;

const StyledInput = styled.textarea`
  width: 100%;
  resize: none;
  height: 8.5em;
  max-height: 8.5em;
  border: none;
  color: ${props => (props.color ? props.color : '#555')};
  font-weight: 500;
  position: absolute;
  top: 0;
  left: 0;
  padding: 3px;
  &::placeholder {
    color: #ccc;
  }
  &:focus {
    outline: none;
  }
`;

const StyledTextarea = styled.div`
  position: relative;
  border: ${props =>
    props.isDirty && props.isRequired && !props.isValid
      ? '2px solid red'
      : '2px solid #d6d8da'};
  border-radius: 5px;
  padding: 10px;
  resize: none;
  height: 11em;
  max-height: 11em;
  color: #555;
  font-weight: 500;
`;

const StyledLabel = styled.label`
  color: #555;
  font-weight: 500;
  display: block;
`;
const StyledIcons = styled.div`
  position: absolute;
  bottom: 0;
  left: 5px;
`;

const StyledErrorMessage = styled.span`
  color: red;
`;

class Textarea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showColorPicker: false,
      isDirty: false,
      error: null,
      color:
        this.props.id === 'infoDescription'
          ? this.props.subgenreId === this.props.infoColor.id
            ? this.props.infoColor.color
            : '#555'
          : this.props.subgenreColor
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      error:
        this.state.isDirty && props.isRequired && !props.value
          ? 'Field is required'
          : null
    });
  }

  componentWillUnmount() {
    this.props.id === 'infoDescription'
      ? this.props.setInfoColor({
          color: this.state.color,
          id: this.props.subgenreId
        })
      : this.props.setNewSubgenreColor(this.state.color);
  }

  /**
   * Handles onChange input event
   * @param evt - value of input
   */
  handleChange = evt => {
    this.props.change({ [this.props.id]: evt });
    this.setState({
      isDirty: true,
      error:
        this.props.isRequired && !this.props.value ? 'Field is required' : null
    });
  };
  
  /**
   * Handles onChange colorpicker event
   * @param evt - hex color value
   */
  handleColorChange = evt => {
    this.setState({ color: evt.hex });
  };

  /**Handles visibility of colorpicker */
  handleColorpickerDisplay = () => {
    this.setState({
      showColorPicker: !this.state.showColorPicker
    });
  };

  render() {
    return (
      <StyledContainer>
        <StyledLabel>{this.props.placeholder}</StyledLabel>
        <StyledTextarea
          isRequired={this.props.isRequired}
          isDirty={this.state.isDirty}
          isValid={!!this.props.value}
        >
          <DebounceInput
            htmlRequired={this.props.isRequired}
            color={this.state.color}
            element={StyledInput}
            required={this.props.isRequired}
            value={this.props.value ? this.props.value : ''}
            placeholder={this.props.placeholder}
            onChange={evt => this.handleChange(evt.target.value)}
            debounceTimeout={300}
          />

          <StyledIcons>
            <TextFormatIcon onClick={this.handleColorpickerDisplay} />
            <LinkIcon />
            <MentionIcon />
          </StyledIcons>
        </StyledTextarea>
        <StyledErrorMessage>{this.state.error}</StyledErrorMessage>
        {this.state.showColorPicker ? (
          <GithubPicker
            color={this.state.color}
            onChange={this.handleColorChange}
          />
        ) : null}
      </StyledContainer>
    );
  }
}

const mapStateToProps = state => ({
  infoColor: getInfoColor(state),
  subgenreColor: getSubgenreColor(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setInfoColor,
      setNewSubgenreColor
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Textarea);
