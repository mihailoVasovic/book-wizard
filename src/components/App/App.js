import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../Header/Header';
import styled from 'styled-components';

import { fetchGenres } from '../../store/actions/genres';

import { nextStep, previousStep } from '../../store/actions/stepper';
import { getSteps, getStep } from '../../store/reducers/stepper';

import { fetchInformations } from '../../store/actions/information';

import Stepper from '../../containers/Stepper/Stepper';
import Wrapper from '../../containers/Wrapper/Wrapper';
import Footer from '../../containers/Footer/Footer';
import SuccessContainer from '../../containers/SuccessContainer/SuccessContainer';


import {
  getGenre,
  getInfo,
  getSubgenre,
  newSubgenreSelected,
  getForm
} from '../../store/reducers/form';

import ListContainer from '../../containers/ListContainer/ListContainer';
import FormContainer from '../../containers/FormContainer/FormContainer';

const StyledApp = styled.div`
display: inline-block;
`

class App extends React.Component {
  componentDidMount() {
    this.props.fetchGenres();
    this.props.fetchInformations();
  }

  handleList = () => {
    const selectedStep = this.props.steps.find(el => el.selected);
    switch (selectedStep.id) {
      case 'genre':
      case 'subgenre':
        return <ListContainer selectedType={selectedStep.id} />;
      case 'newSubgenre':
      case 'info':
        return <FormContainer selectedType={selectedStep.id} />;

      default:
        return null;
    }
  };

  handleResults = () => {
    alert(JSON.stringify(this.props.form, null, 2));
  };

  render() {
    let content = this.handleList();

    return (
      <StyledApp>
        <Wrapper padding="20px 40px 20px 40px">
          <Header />
          <Stepper items={this.props.steps} />
          {content}
          <Footer
            handleNext={() =>
              this.props.nextStep({ subgenreId: this.props.subgenre.id })
            }
            steps={this.props.steps}
            showResults={this.handleResults}
            handlePrevious={this.props.previousStep}
            step={this.props.step}
            genre={this.props.genre}
            subgenre={this.props.subgenre}
            info={this.props.info}
            newSubgenreSelected={this.props.newSubgenreSelected}
          />
          {/* <SuccessContainer /> */}
        </Wrapper>
      </StyledApp>
    );
  }
}

const mapStateToProps = state => ({
  steps: getSteps(state),
  genre: getGenre(state),
  subgenre: getSubgenre(state),
  info: getInfo(state),
  step: getStep(state),
  newSubgenreSelected: newSubgenreSelected(state),
  form: getForm(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchGenres,
      nextStep,
      previousStep,
      fetchInformations
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
