import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  padding: ${props => (props.padding ? props.padding : '16px')};
  margin: ${props => (props.margin ? props.margin : '0')};
  background-color: ${props => (props.background ? props.background : '#fff')};
  color: ${props => (props.color ? props.color : 'inherit')};
  border: 1px solid #ddd;
  border-radius: 5px;
  width: ${props => (props.width ? props.width : 'inherit')};
  cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  &:disabled {
    color: ${props => (props.color ? props.color : '#ccc')};
  }
`;

const Button = props => {
  return (
    <StyledButton
      padding={props.padding}
      margin={props.margin}
      background={props.background}
      color={props.selected ? '#e8e8e8' : props.color}
      onClick={() => props.click()}
      disabled={props.disabled}
      width={props.width}
    >
      {props.name}
    </StyledButton>
  );
};

export default Button;
