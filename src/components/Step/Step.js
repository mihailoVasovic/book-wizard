import React from 'react';
import styled from 'styled-components';

const StyledStep = styled.div`
  display: inline-block;
  position: relative;
  padding: 0 60px;
  font-size: 18px;
  &:first-child {
    padding: 0 60px 0 0;
  }
  &:last-child {
    padding: 0 0 0 60px;
  }
`;

const StyledBar = styled.div`
  top: 45%;
  position: absolute;
  height: 1px;
  border-top: 1px solid #c7c7c7;
`;

const StyledLeftBar = styled(StyledBar)`
  left: 0;
  right: 70%;
  ${StyledStep}:last-child & {
    right: 50%;
    left: 0;
  }
  ${StyledStep}:first-child & {
    right: 100%;
    left: 0;
  }
`;
const StyledRightBar = styled(StyledBar)`
  left: 70%;
  right: 0;
  ${StyledStep}:first-child & {
    left: 50%;
    right: 0;
  }
  ${StyledStep}:last-child & {
    left: 100%;
    right: 0;
  }
`;

const StyledCircle = styled.div`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  background-color: ${props => (props.active ? '#515a65' : '#e7eaef;')}
  text-align: center;
  line-height: 50px;
  color: ${props => (props.active ? '#e8e8e8' : 'inherit')};
`;

const StyledTitle = styled.div`
  font-size: 16px;
  font-weight: 600;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  ${StyledStep}:first-child & {
    left: 0;
    transform: translateX(0);
  }
  ${StyledStep}:last-child & {
    right: 0;
    left: 40%;
    transform: translateX(0);
  }
`;

const Step = props => {
  return (
    <StyledStep>
      <StyledCircle active={props.selected ? true : false}>
        {props.step}
      </StyledCircle>
      {props.index !== 0 ? <StyledLeftBar /> : ''}
      {props.index !== props.length - 1 ? <StyledRightBar /> : ''}
      <StyledTitle>{props.name}</StyledTitle>
    </StyledStep>
  );
};

export default Step;
